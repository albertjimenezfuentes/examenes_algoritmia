from Entregable_3.bt_scheme import BacktrackingVCSolver, PartialSolutionWithVisitedControl


class SumaDosEnterosPS(PartialSolutionWithVisitedControl):
    def __init__(self,conjunto,  C: "Set", A: "Integer", B: "Integer", Candidata_A=set(), Candidata_B=set()):
        self.C = set(C)
        self.conjunto = C
        self.A = A
        self.B = B
        self.candidata_A = set(Candidata_A)
        self.candidata_B = set(Candidata_B)

    def is_solution(self) -> "bool":
        return (sum(self.candidata_A) == self.A and sum(self.candidata_B)) == self.B

    def get_solution(self) -> "solution":
        return sum(self.candidata_A), sum(self.candidata_B)

    def successors(self) -> "IEnumerable<PartialSolution>":
        # Turno de A
        if sum(self.candidata_A) != self.A:
            for elemX in self.conjunto:
                self.candidata_A.add(elemX)
                for elemY in self.conjunto:
                    if elemX + elemY <= self.A:
                        self.candidata_A.add(elemY)
                        self.C -= self.candidata_A
                        yield SumaDosEnterosPS(self.conjunto,self.C, self.A, self.B, self.candidata_A, self.candidata_B)
                    else:
                        self.candidata_A.remove(elemX)
                        break

        if sum(self.candidata_B) != self.B:
            # Turno de B
            for elemX in self.conjunto:
                self.candidata_B.add(elemX)
                for elemY in self.conjunto:
                    if elemX + elemY <= self.B:
                        self.candidata_B.add(elemY)
                        self.C -= self.candidata_B
                        yield SumaDosEnterosPS(self.conjunto,self.C, self.A, self.B, self.candidata_A, self.candidata_B)
                    else:
                        self.candidata_B.remove(elemX)
                        break
        #print(sum(self.candidata_A),sum(self.candidata_B))

    def state(self) -> "state":
        return tuple(self.candidata_A), tuple(self.candidata_B)


# Main:
CONJUNTO = [1, 2, 3, 4, 5, 7, 8, 10]
CONJUNTO = sorted(CONJUNTO, reverse=True)
NUMERO_A = 10
NUMERO_B = 40
initialPS = SumaDosEnterosPS(CONJUNTO, CONJUNTO,NUMERO_A,NUMERO_B)
for res in BacktrackingVCSolver.solve(initialPS):
    print(res)

